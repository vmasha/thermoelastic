#include <dolfin.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Heat.h"
#include "Elasticity.h"
using namespace dolfin;

int main(int argc, char** argv){
    PetscInitialize(&argc, &argv, (char*)0, "");
    parameters["reorder_dofs_serial"] = false;
    parameters["allow_extrapolation"] = true;
    Timer timer1("ms");
    Timer timer("ms");
    timer.start();

    // --------------------
    //      PARAMETERS
    // --------------------
    double Tinit = std::atof(argv[1]); 
    double TTop  = std::atof(argv[2]); 
    double tmax  = std::atof(argv[3]); 
    int tcount   = std::atoi(argv[4]); 

    std::string meshname  = argv[5]; 
    std::string resultDir = argv[6];

    int del = std::atoi(argv[7]);

    double valp1 = 14.0, valp10 = 20.0;
    double THot = 10.0;
    info("thm");

    double tau = tmax/tcount;
    auto dt = std::make_shared<Constant>(tau);
    auto alp1  = std::make_shared<Constant>(valp1);
    auto alp10 = std::make_shared<Constant>(valp10);
    auto g1  = std::make_shared<Constant>(TTop);
    auto g10 = std::make_shared<Constant>(THot);
    auto g0 = std::make_shared<Constant>(0.0);

    int NLmaxiter = std::atoi(argv[8]); 
    double NLrtol = std::atof(argv[9]); 
    std::string outfilename = argv[10];

    // --------------------
    //         MESH
    // --------------------
    auto mesh = std::make_shared<Mesh>(meshname + ".xml");  
    auto bounds = std::make_shared<MeshFunction<size_t> >(mesh, meshname + "_facet_region.xml");  
    auto subdom = std::make_shared<MeshFunction<size_t> >(mesh, meshname + "_physical_region.xml");  
    mesh->init(); info(*mesh);
    int dim  = mesh->geometry().dim();

    // --------------------
    //         HEAT
    // --------------------
    auto V = std::make_shared<Heat::Form_a::TrialSpace>(mesh);
    Heat::Form_a a(V, V);
    Heat::Form_L L(V);
    auto T = std::make_shared<Function>(V);
    auto T0 = std::make_shared<Function>(V);
    auto Tn = std::make_shared<Function>(V);
    int NT = T->vector().get()->size();
    PETScMatrix A;
    PETScVector b(MPI_COMM_SELF, NT);
    a.dx = subdom; a.ds = bounds;
    L.dx = subdom; L.ds = bounds;
    a.dt = dt;
    a.alp1 = alp1;
    a.alp10 = alp10;
    L.dt = dt;
    L.alp1 = alp1;
    L.alp10 = alp10;
    L.g1 = g1;
    L.g10 = g10;


    // --------------------
    //   NONLINEAR PARAMS
    // --------------------
    auto fw  = std::make_shared<Function>(V);
    auto fti = std::make_shared<Function>(V); auto ftw = std::make_shared<Function>(V);
    auto fn  = std::make_shared<Function>(V); 
    auto fdn = std::make_shared<Function>(V); auto fdn2 = std::make_shared<Function>(V);
    Heat::Form_af af(V, V);
    PETScMatrix Af;
    assemble(Af, af);
    Heat::Form_fw Lw(V); Heat::Form_fti Lti(V); Heat::Form_ftw Ltw(V);
    Heat::Form_fn Ln(V); Heat::Form_fdn Ldn(V); Heat::Form_fdn2 Ldn2(V);
    PETScVector bn(MPI_COMM_SELF, NT), bdn(MPI_COMM_SELF, NT), bdn2(MPI_COMM_SELF, NT);
    PETScVector bw(MPI_COMM_SELF, NT), bti(MPI_COMM_SELF, NT), btw(MPI_COMM_SELF, NT);
    Lw.dx = subdom; Lti.dx = subdom; Ltw.dx = subdom;
    Ln.dx = subdom; Ldn.dx = subdom; Ldn2.dx = subdom;


    // --------------------
    //      ELASTICITY
    // --------------------
    auto W = std::make_shared<Elasticity::Form_a::TrialSpace>(mesh);
    Elasticity::Form_a au(W, W);
    Elasticity::Form_L Lu(W);
    auto bc1 = std::make_shared<DirichletBC>(W->sub(0), g0, bounds, 4);
    auto bc2 = std::make_shared<DirichletBC>(W->sub(0), g0, bounds, 3);
    auto bc3 = std::make_shared<DirichletBC>(W->sub(1), g0, bounds, 2);
    auto bc4 = std::make_shared<DirichletBC>(W->sub(1), g0, bounds, 5);
    std::shared_ptr<DirichletBC> bc5;
    if (dim == 3)
        bc5 = std::make_shared<DirichletBC>(W->sub(2), g0, bounds, 6);
    auto u = std::make_shared<Function>(W);
    int Nu = u->vector().get()->size();
    PETScMatrix Au;
    PETScVector bu(MPI_COMM_SELF, Nu);
    au.dx = subdom; au.ds = bounds;
    Lu.dx = subdom; Lu.ds = bounds;


    // ------------------------------
    //       INITIAL CONDITIONS
    // ------------------------------
    int incrdisp = Nu - NT;
    for(int i = 0; i < NT; i++)
        T->vector().get()->setitem(i, Tinit);
    T->vector().get()->apply("insert");

    // --------------------
    //        SOLVE
    // --------------------
    LinearSolver lsolver1, lsolver2;
    File file1(resultDir + "thm-T.pvd");
    File file2(resultDir + "thm-u.pvd");
    File file4(resultDir + "thm-n.pvd");
    File file7(resultDir + "thm-ti.pvd");
    File file8(resultDir + "thm-tw.pvd");
    File file9(resultDir + "thm-w.pvd");
    double stime = 0.0;
    int ni, scounter = 0;
    remove(outfilename.data());
    std::ofstream outfileIter(outfilename, std::ios_base::app); 
    double t = 0;
    for(int ti  = 1; ti < (tcount + 1); ti++){
        t += tau;
        // ----- heat -----
        for(int i = 0; i < NT; i++){
            T0->vector().get()->setitem(i, T->vector().get()->getitem(i));
            Tn->vector().get()->setitem(i, T->vector().get()->getitem(i));
        }
        T0->vector().get()->apply("insert");
        Tn->vector().get()->apply("insert");
        L.T0 = T0;
        // Picard iterations for heat 
        for(ni = 1; ni < NLmaxiter+1; ni++){
            a.Tn = T;
            L.Tn = T;
            assemble(A, a);
            assemble(b, L);
            std::size_t iters1 = lsolver1.solve(A, *T->vector(), b);
            // difference between solutions
            double diffn = 0.0, sumn = 0.0;
            for(int i = 0; i < NT; i++){
                double val1 = Tn->vector().get()->getitem(i);
                double val2 = T->vector().get()->getitem(i);
                diffn += std::pow(val1 - val2, 2);
                sumn += std::pow(val1, 2);
                Tn->vector().get()->setitem(i, val2);
            }
            Tn->vector().get()->apply("insert");
            double nlerr = std::sqrt(diffn/sumn)*100;
            info("heat nl-iter %d with rtol = %g (%g, %g) (lin iters = %d)", ni, nlerr, diffn, sumn, iters1);
            if (nlerr < NLrtol or ti == 1 or NLmaxiter == 1)
                break;
        }
        
        // ----- elasticity -----
        Lu.T  = T;
        Lu.Tn = T0;
        Lu.un = u;
        assemble(Au, au);
        assemble(bu, Lu);
        bc1->apply(Au, bu);
        bc2->apply(Au, bu);
        bc3->apply(Au, bu);
        if (dim == 3){
            bc4->apply(Au, bu);
            bc5->apply(Au, bu);
        }
        std::size_t iters2 = lsolver2.solve(Au, *u->vector(), bu);

        // ----- save -----
        info("%d: solve T(%g, %g) with %d nl iter", ti, T->vector().get()->min(), T->vector().get()->max(), ni);
        outfileIter << ti << " " << t << " " << ni << "\n";
        if (ti%(tcount/del) == 0){
            timer1.start();
            info("    solve u(%g, %g) with %d lin iter\n",     u->vector().get()->min(), u->vector().get()->max(), iters2);
            file1 << *T;
            file2 << *u;
            File file1b(resultDir + "thm-T" + std::to_string(scounter) + ".xml");
            File file2b(resultDir + "thm-u" + std::to_string(scounter) + ".xml");
            file1b << *T;
            file2b << *u;
            // ---- nonlinear params ------
            Lw.Tn = T; Lti.Tn = T; Ltw.Tn = T; Ln.Tn = T;
            assemble(bn,   Ln);     lsolver1.solve(Af, *fn->vector(), bn);      file4 << *fn;
            assemble(bti,  Lti);    lsolver1.solve(Af, *fti->vector(), bti);    file7 << *fti;
            assemble(btw,  Ltw);    lsolver1.solve(Af, *ftw->vector(), btw);    file8 << *ftw;
            assemble(bw,   Lw);     lsolver1.solve(Af, *fw->vector(), bw);      file9 << *fw;
            scounter++;
            stime += timer1.stop();
        }
    }
    outfileIter.close();

    info("Solution TIME  %f with saving time %f (%d times)", timer.stop(), stime, scounter);
    return 0;
}