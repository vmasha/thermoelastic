# Heat transfer and deformations of soils with phase change

In this work, we present a mathematical model of heat transfer and  mechanics of soils with phase change.  The mathematical model is based on definition of water and ice fraction volumes as a function of temperature. In the presented mathematical model, the soil deformations occur due to the porosity growth followed by difference between ice and water density. 

Implementation based on the [FEniCS](https://fenicsproject.org). 

Please cite the paper: Numerical simulation of heat transfer and deformations of soils with phase change. Maria Vasilyeva, Vasily Vasil'ev.


## Citation

    @article{vas2020thermoelastic,
      title={Numerical simulation of heat transfer and deformations of soils with phase change},
      author={Maria Vasilyeva, Vasily Vasil'ev},
      journal={submitted},
      year={2020}
    }

## How to use

1. Generate grids for geometries in ./data folder using [GMSH](https://gmsh.info)

2. Run C++ solver:
	* ffc -l dolfin Heat.ufl 
	* ffc -l dolfin Elasticity.ufl 
	* copy CMakeList.txt from FEniCS demo folder in our computer and compile the project
	* ./demo_poisson 2.0 -15.0 1.0e6 200 ../data/mesh1 ./results/ 10 30 1.0 ./results/nliters

