LX = 6.0;
LY = 6.0;
LZ = 3.0;
//p = LX/40;
p = LX/10;
p2 = p/3;

dx = LX/3;
dy = LY/2;

hx = LX/4;
hy = LY/5;

Point(1) = {0,   0, 0, p};
Point(2) = {0,  LY, 0, p};
Point(3) = {LX, LY, 0, p};
Point(4) = {LX,  0, 0, p};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(5) = {3, 4};
Line(6) = {4, 1};

Point(11) = {0,   0, LZ, p2};
Point(12) = {0,  LY, LZ, p2};
Point(13) = {LX, LY, LZ, p2};
Point(14) = {LX,  0, LZ, p2};

Line(21) = {11, 12};
Line(22) = {12, 13};
Line(25) = {13, 14};
Line(26) = {14, 11};

Line(31) = {1, 11};
Line(32) = {2, 12};
Line(35) = {3, 13};
Line(36) = {4, 14};


//
Point(40) = {0+dx, 0+dy, LZ, p2};
Point(41) = {0+dx+hx, 0+dy, LZ, p2};
Point(42) = {0+dx-hx, 0+dy, LZ, p2};
Point(43) = {0+dx, 0+dy+hy, LZ, p2};
Point(44) = {0+dx, 0+dy-hy, LZ, p2};


Ellipse(37) = {42, 40, 41, 43};
Ellipse(38) = {43, 40, 44, 41};
Ellipse(39) = {41, 40, 42, 44};
Ellipse(40) = {44, 40, 43, 42};


Line Loop(1) = {37, 38, 39, 40};
Plane Surface(1) = {1};
Line Loop(2) = {22, 25, 26, 21};
Plane Surface(2) = {1, 2};
Line Loop(3) = {26, -31, -6, 36};
Plane Surface(3) = {3};
Line Loop(4) = {36, -25, -35, 5};
Plane Surface(4) = {4};
Line Loop(5) = {22, -35, -2, 32};
Plane Surface(5) = {5};
Line Loop(6) = {1, 32, -21, -31};
Plane Surface(6) = {6};
Line Loop(7) = {2, 5, 6, 1};
Plane Surface(7) = {7};
Surface Loop(1) = {6, 7, 5, 2, 1, 3, 4};

Volume(1) = {1};


Physical Surface(3) = {4};
Physical Surface(4) = {6};

Physical Surface(2) = {5};
Physical Surface(5) = {3};

Physical Surface(6) = {7};

Physical Surface(1) = {2};
Physical Surface(10) = {1};

Physical Volume(1) = {1};
