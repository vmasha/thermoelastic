LX = 6.0;
LY = 6.0;
LZ = 3.0;
//p = LX/40;
p = LX/10;
p2 = p/3;

Point(1) = {0,   0, 0, p};
Point(2) = {0,  LY, 0, p};
Point(3) = {LX, LY, 0, p};
Point(4) = {LX,  0, 0, p};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(5) = {3, 4};
Line(6) = {4, 1};

Point(11) = {0,   0, LZ, p2};
Point(12) = {0,  LY, LZ, p2};
Point(13) = {LX, LY, LZ, p2};
Point(14) = {LX,  0, LZ, p2};

Line(21) = {11, 12};
Line(22) = {12, 13};
Line(25) = {13, 14};
Line(26) = {14, 11};

Line(31) = {1, 11};
Line(32) = {2, 12};
Line(35) = {3, 13};
Line(36) = {4, 14};


//1
d1 = LX*2/3;
d2 = 0.5;
r1 = 0.3;
r2 = 0.3;

Point(40) = {0+d1, 	0, LZ-d2, p2};
Point(41) = {0+d1+r1, 	0, LZ-d2, p2};
Point(42) = {0+d1-r1, 	0, LZ-d2, p2};
Point(43) = {0+d1, 	0, LZ-d2+r2, p2};
Point(44) = {0+d1, 	0, LZ-d2-r2, p2};

Ellipse(47) = {42, 40, 41, 43};
Ellipse(48) = {43, 40, 44, 41};
Ellipse(49) = {41, 40, 42, 44};
Ellipse(40) = {44, 40, 43, 42};

Point(50) = {0+d1, 	LY, LZ-d2, p2};
Point(51) = {0+d1+r1, 	LY, LZ-d2, p2};
Point(52) = {0+d1-r1, 	LY, LZ-d2, p2};
Point(53) = {0+d1, 	LY, LZ-d2+r2, p2};
Point(54) = {0+d1, 	LY, LZ-d2-r2, p2};

Ellipse(57) = {52, 50, 51, 53};
Ellipse(58) = {53, 50, 54, 51};
Ellipse(59) = {51, 50, 52, 54};
Ellipse(50) = {54, 50, 53, 52};

hh = 0.05;
r1 = r1+hh;
r2 = r2+hh;
Point(60) = {0+d1, 	0, LZ-d2, p2};
Point(61) = {0+d1+r1, 	0, LZ-d2, p2};
Point(62) = {0+d1-r1, 	0, LZ-d2, p2};
Point(63) = {0+d1, 	0, LZ-d2+r2, p2};
Point(64) = {0+d1, 	0, LZ-d2-r2, p2};

Ellipse(67) = {62, 60, 61, 63};
Ellipse(68) = {63, 60, 64, 61};
Ellipse(69) = {61, 60, 62, 64};
Ellipse(60) = {64, 60, 63, 62};

Point(70) = {0+d1, 	LY, LZ-d2, p2};
Point(71) = {0+d1+r1, 	LY, LZ-d2, p2};
Point(72) = {0+d1-r1, 	LY, LZ-d2, p2};
Point(73) = {0+d1, 	LY, LZ-d2+r2, p2};
Point(74) = {0+d1, 	LY, LZ-d2-r2, p2};

Ellipse(77) = {72, 70, 71, 73};
Ellipse(78) = {73, 70, 74, 71};
Ellipse(79) = {71, 70, 72, 74};
Ellipse(70) = {74, 70, 73, 72};




//2
d1 = LX/2;
d2 = 0.4;
r1 = 0.15;
r2 = 0.15;
Point(140) = {0, 0+d1, 	 LZ-d2, p2};
Point(141) = {0, 0+d1+r1,LZ-d2, p2};
Point(142) = {0, 0+d1-r1,LZ-d2, p2};
Point(143) = {0, 0+d1, 	 LZ-d2+r2, p2};
Point(144) = {0, 0+d1, 	 LZ-d2-r2, p2};

Ellipse(147) = {142, 140, 141, 143};
Ellipse(148) = {143, 140, 144, 141};
Ellipse(149) = {141, 140, 142, 144};
Ellipse(140) = {144, 140, 143, 142};

Point(150) = {0+d1, 	0, LZ-d2, p2};
Point(151) = {0+d1+r1, 	0, LZ-d2, p2};
Point(152) = {0+d1-r1, 	0, LZ-d2, p2};
Point(153) = {0+d1, 	0, LZ-d2+r2, p2};
Point(154) = {0+d1, 	0, LZ-d2-r2, p2};

Ellipse(157) = {152, 150, 151, 153};
Ellipse(158) = {153, 150, 154, 151};
Ellipse(159) = {151, 150, 152, 154};
Ellipse(150) = {154, 150, 153, 152};

hh = 0.05;
r1 = r1+hh;
r2 = r2+hh;
Point(160) = {0, 0+d1, 	LZ-d2, p2};
Point(161) = {0, 0+d1+r1, LZ-d2, p2};
Point(162) = {0, 0+d1-r1, LZ-d2, p2};
Point(163) = {0, 0+d1, 	LZ-d2+r2, p2};
Point(164) = {0, 0+d1, 	LZ-d2-r2, p2};

Ellipse(167) = {162, 160, 161, 163};
Ellipse(168) = {163, 160, 164, 161};
Ellipse(169) = {161, 160, 162, 164};
Ellipse(160) = {164, 160, 163, 162};

Point(170) = {0+d1, 	0, LZ-d2, p2};
Point(171) = {0+d1+r1, 	0, LZ-d2, p2};
Point(172) = {0+d1-r1, 	0, LZ-d2, p2};
Point(173) = {0+d1, 	0, LZ-d2+r2, p2};
Point(174) = {0+d1, 	0, LZ-d2-r2, p2};

Ellipse(177) = {172, 170, 171, 173};
Ellipse(178) = {173, 170, 174, 171};
Ellipse(179) = {171, 170, 172, 174};
Ellipse(170) = {174, 170, 173, 172};


Line(180) = {63, 73};
Line(181) = {43, 53};
Line(182) = {71, 61};
Line(183) = {41, 51};
Line(184) = {54, 44};
Line(185) = {64, 74};
Line(186) = {52, 42};
Line(187) = {62, 72};

Line(188) = {164, 174};
Line(189) = {154, 144};
Line(190) = {162, 172};
Line(191) = {152, 142};
Line(192) = {143, 153};
Line(193) = {173, 163};
Line(194) = {141, 151};
Line(195) = {171, 161};

Line Loop(1) = {191, 147, 192, -157};
Surface(1) = {1};
Line Loop(2) = {158, -194, -148, 192};
Surface(2) = {2};
Line Loop(3) = {194, 159, 189, -149};
Surface(3) = {3};
Line Loop(4) = {189, 140, -191, -150};
Surface(4) = {4};
Line Loop(5) = {190, -170, -188, 160};
Surface(5) = {5};
Line Loop(6) = {179, -188, -169, -195};
Surface(6) = {6};
Line Loop(7) = {168, -195, -178, 193};
Surface(7) = {7};
Line Loop(8) = {193, -167, 190, 177};
Surface(8) = {8};
Line Loop(9) = {48, 183, -58, -181};
Surface(9) = {9};
Line Loop(10) = {57, -181, -47, -186};
Surface(10) = {10};
Line Loop(11) = {40, -186, -50, 184};
Surface(11) = {11};
Line Loop(12) = {59, 184, -49, 183};
Surface(12) = {12};
Line Loop(13) = {69, 185, -79, 182};
Surface(13) = {13};
Line Loop(14) = {182, -68, 180, 78};
Surface(14) = {14};
Line Loop(15) = {67, 180, -77, -187};
Surface(15) = {15};
Line Loop(16) = {60, 187, -70, -185};
Surface(16) = {16};


Line Loop(17) = {26, 21, 22, 25};
Surface(17) = {17};

Line Loop(18) = {1, 2, 5, 6};
Surface(18) = {18};

Line Loop(20) = {21, -32, -1, 31};
Line Loop(21) = {167, 168, 169, 160};
Surface(20) = {20, 21};

Line Loop(24) = {22, -35, -2, 32};
Line Loop(25) = {78, 79, 70, 77};
Surface(22) = {24, 25};

Line Loop(27) = {177, 178, 179, 170};
Line Loop(22) = {31, -26, -36, 6};
Line Loop(23) = {68, 69, 60, 67};
Surface(21) = {22, 23, 27};

Line Loop(26) = {25, -36, -5, 35};
Surface(23) = {26};

Line Loop(28) = {150, 157, 158, 159};
Surface(24) = {27, 28};

Line Loop(29) = {140, 147, 148, 149};
Surface(25) = {21, 29};

Line Loop(30) = {40, 47, 48, 49};
Surface(26) = {23, 30};

Line Loop(31) = {58, 59, 50, 57};
Surface(27) = {25, 31};

Surface Loop(1) = {24, 3, 2, 25, 1, 4, 8, 7, 6, 5};
Volume(1) = {1};

Surface Loop(2) = {26, 12, 27, 10, 9, 11, 15, 14, 13, 16};
Volume(2) = {2};

Surface Loop(3) = {17, 21, 20, 22, 23, 18, 15, 14, 13, 16, 5, 8, 7, 6};
Volume(3) = {3};



Physical Volume(1) = {3};
Physical Volume(2) = {1, 2};

Physical Surface(1) = {17};
Physical Surface(10) = {12, 9, 10, 11, 1, 2, 4, 3};

Physical Surface(6) = {18};

Physical Surface(3) = {23};
Physical Surface(4) = {20};

Physical Surface(5) = {21};
Physical Surface(2) = {22};

