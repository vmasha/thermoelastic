LX = 6.0;
LY = 3.0;
p = LX/50;
p2 = p/5;

dy = 0.8;
h = LX/3;
d = 0.6;

Point(1) = {0, 0, 0, p};
Point(2) = {0, LY, 0, p2};
Point(3) = {0+h,   LY, 0, p2};
Point(4) = {0+h+d, LY, 0, p2};
Point(5) = {LX, LY, 0, p2};
Point(6) = {LX, 0, 0, p};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 1};

Point(11) = {0+h,   LY-dy, 0, p2};
Point(12) = {0+h+d, LY-dy, 0, p2};

Point(13) = {0+h,   LY+dy, 0, p2};
Point(14) = {0+h+d, LY+dy, 0, p2};

Line(7) = {13, 3};
Line(8) = {3, 11};
Line(9) = {11, 12};
Line(10) = {12, 4};
Line(11) = {4, 14};
Line(12) = {14, 13};

Line Loop(1) = {2, 8, 9, 10, 4, 5, 6, 1};
Plane Surface(1) = {1};

Line Loop(2) = {8, 9, 10, 11, 12, 7};
Plane Surface(2) = {2};

Physical Line(10) = {2};
Physical Line(1) = {7, 12, 11, 4};
Physical Line(2) = {6};
Physical Line(3) = {5};
Physical Line(4) = {1};

Physical Surface(1) = {1};
Physical Surface(2) = {2};
