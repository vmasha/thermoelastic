LX = 6.0;
LY = 3.0;
p = LX/50;
p2 = p/5;



Point(1) = {0, 0, 0, p};
Point(2) = {LX*1/5, LY, 0, p2};
Point(3) = {LX*2/5, LY, 0, p2};
Point(4) = {0, LY, 0, p2};
Point(5) = {LX, LY, 0, p2};
Point(6) = {LX, 0, 0, p};

//+
Line(1) = {1, 4};
//+
Line(2) = {4, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 1};

Line Loop(1) = {1, 2, 3, 4, 5, 6};
Plane Surface(1) = {1};

Physical Surface(1) = {1};
//+
Physical Line(1) = {2, 4};
//+
Physical Line(2) = {6};
//+
Physical Line(3) = {5};
//+
Physical Line(4) = {1};
//+
Physical Line(10) = {3};
