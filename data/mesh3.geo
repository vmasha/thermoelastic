LX = 6.0;
LY = 3.0;
p = LX/50;
p2 = p/5;

h = 0.6;
d = 1.0;
rp = 0.05;
r1 = 0.15;
r2 = 0.3;


Point(1) = {0, 0, 0, p};
Point(2) = {0, LY, 0, p2};
Point(3) = {LX, LY, 0, p2};
Point(4) = {LX, 0, 0, p};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// 1
Point(11) = {LX/2+d, LY-h, 0, p2};
Point(12) = {LX/2+d-r1, LY-h, 0, p2};
Point(13) = {LX/2+d+r1, LY-h, 0, p2};
Point(14) = {LX/2+d, LY-h-r1, 0, p2};
Point(15) = {LX/2+d, LY-h+r1, 0, p2};
Circle(11) = {15, 11, 13};
Circle(12) = {13, 11, 14};
Circle(13) = {14, 11, 12};
Circle(14) = {12, 11, 15};

Point(21) = {LX/2+d, LY-h, 0, p2};
Point(22) = {LX/2+d-r1-rp, LY-h, 0, p2};
Point(23) = {LX/2+d+r1+rp, LY-h, 0, p2};
Point(24) = {LX/2+d, LY-h-r1-rp, 0, p2};
Point(25) = {LX/2+d, LY-h+r1+rp, 0, p2};
Circle(21) = {25, 21, 23};
Circle(22) = {23, 21, 24};
Circle(23) = {24, 21, 22};
Circle(24) = {22, 21, 25};


// 2
Point(31) = {LX/2-d, LY-h, 0, p2};
Point(32) = {LX/2-d-r2, LY-h, 0, p2};
Point(33) = {LX/2-d+r2, LY-h, 0, p2};
Point(34) = {LX/2-d, LY-h-r2, 0, p2};
Point(35) = {LX/2-d, LY-h+r2, 0, p2};
Circle(31) = {35, 31, 33};
Circle(32) = {33, 31, 34};
Circle(33) = {34, 31, 32};
Circle(34) = {32, 31, 35};

Point(41) = {LX/2-d, LY-h, 0, p2};
Point(42) = {LX/2-d-r2-rp, LY-h, 0, p2};
Point(43) = {LX/2-d+r2+ rp, LY-h, 0, p2};
Point(44) = {LX/2-d, LY-h-r2-rp, 0, p2};
Point(45) = {LX/2-d, LY-h+r2+rp, 0, p2};
Circle(41) = {45, 41, 43};
Circle(42) = {43, 41, 44};
Circle(43) = {44, 41, 42};
Circle(44) = {42, 41, 45};


Line Loop(1) = {1, 2, 3, 4};

Line Loop(10) = {14, 11, 12, 13};
Line Loop(11) = {24, 21, 22, 23};

Line Loop(20) = {31, 32, 33, 34};
Line Loop(21) = {44, 41, 42, 43};

Plane Surface(1) = {1, 11, 21};
Plane Surface(2) = {10, 11};
Plane Surface(3) = {20, 21};


Physical Surface(1) = {1};
Physical Surface(2) = {2, 3};


Physical Line(1) = {2};

Physical Line(2) = {4};
Physical Line(3) = {3};
Physical Line(4) = {1};

Physical Line(10) = {34, 31, 32, 33, 14, 11, 12, 13};
