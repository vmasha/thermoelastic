LX = 6.0;
LY = 6.0;
LZ = 3.0;
//p = LX/40;
p = LX/10;
p2 = p/3;

dx = LX/3;
dy = LY/2;

hx = 0.3;
hy = 0.3;
hz = 0.8;

Point(1) = {0,   0, 0, p};
Point(2) = {0,  LY, 0, p};
Point(3) = {LX, LY, 0, p};
Point(4) = {LX,  0, 0, p};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(5) = {3, 4};
Line(6) = {4, 1};

Point(11) = {0,   0, LZ, p2};
Point(12) = {0,  LY, LZ, p2};
Point(13) = {LX, LY, LZ, p2};
Point(14) = {LX,  0, LZ, p2};

Point(1112) = {0,   dy, LZ, p2};
Point(1114) = {dx,  0, LZ, p2};

Line(21) = {11, 1112};
Line(28) = {1112, 12};

Line(22) = {12, 13};
Line(25) = {13, 14};

Line(26) = {14, 1114};
Line(27) = {1114, 11};

Line(31) = {1, 11};
Line(32) = {2, 12};
Line(35) = {3, 13};
Line(36) = {4, 14};


//1
Point(40) = {0+dx, 0+dy, LZ, p2};
Point(41) = {0+dx+hx, 0+dy, LZ, p2};
Point(42) = {0+dx-hx, 0+dy, LZ, p2};
Point(43) = {0+dx, 0+dy+hy, LZ, p2};
Point(44) = {0+dx, 0+dy-hy, LZ, p2};
Ellipse(37) = {42, 40, 41, 43};
Ellipse(38) = {43, 40, 44, 41};
Ellipse(39) = {41, 40, 42, 44};
Ellipse(40) = {44, 40, 43, 42};
//2
Point(140) = {0+dx, 0+dy,    LZ-hz, p2};
Point(141) = {0+dx+hx, 0+dy, LZ-hz, p2};
Point(142) = {0+dx-hx, 0+dy, LZ-hz, p2};
Point(143) = {0+dx, 0+dy+hy, LZ-hz, p2};
Point(144) = {0+dx, 0+dy-hy, LZ-hz, p2};
Ellipse(137) = {142, 140, 141, 143};
Ellipse(138) = {143, 140, 144, 141};
Ellipse(139) = {141, 140, 142, 144};
Ellipse(140) = {144, 140, 143, 142};
//3
Point(240) = {0+dx, 0+dy,    LZ+hz, p2};
Point(241) = {0+dx+hx, 0+dy, LZ+hz, p2};
Point(242) = {0+dx-hx, 0+dy, LZ+hz, p2};
Point(243) = {0+dx, 0+dy+hy, LZ+hz, p2};
Point(244) = {0+dx, 0+dy-hy, LZ+hz, p2};
Ellipse(237) = {242, 240, 241, 243};
Ellipse(238) = {243, 240, 244, 241};
Ellipse(239) = {241, 240, 242, 244};
Ellipse(240) = {244, 240, 243, 242};



Line Loop(3) = {26, 27, -31, -6, 36};
Plane Surface(3) = {3};
Line Loop(4) = {36, -25, -35, 5};
Plane Surface(4) = {4};
Line Loop(5) = {22, -35, -2, 32};
Plane Surface(5) = {5};
Line Loop(6) = {1, 32, -21,-28, -31};
Plane Surface(6) = {6};
Line Loop(7) = {2, 5, 6, 1};
Plane Surface(7) = {7};
Surface Loop(1) = {6, 7, 5, 2, 1, 3, 4};

Line(241) = {144, 44};
Line(242) = {44, 244};
Line(243) = {242, 42};
Line(244) = {42, 142};
Line(245) = {141, 41};
Line(246) = {41, 241};
Line(247) = {243, 43};
Line(248) = {43, 143};

Line(250) = {42, 1112};
Line(251) = {44, 1114};

Line Loop(8) = {251, 27, 21, -250, -40};
Plane Surface(8) = {8};
Line Loop(9) = {26, -251, -39, -38, -37, 250, 28, 22, 25};
Plane Surface(9) = {9};

Line Loop(10) = {237, 238, 239, 240};
Plane Surface(10) = {10};
Line Loop(11) = {137, 138, 139, 140};
Plane Surface(11) = {11};

Line Loop(12) = {246, -238, 247, 38};
Surface(12) = {12};
Line Loop(13) = {38, -245, -138, -248};
Surface(13) = {13};
Line Loop(14) = {137, -248, -37, 244};
Surface(14) = {14};
Line Loop(15) = {37, -247, -237, 243};
Surface(15) = {15};
Line Loop(16) = {243, -40, 242, 240};
Surface(16) = {16};
Line Loop(17) = {40, 244, -140, 241};
Surface(17) = {17};
Line Loop(18) = {39, 242, -239, -246};
Surface(18) = {18};
Line Loop(19) = {39, -241, -139, 245};
Surface(19) = {19};


Physical Surface(3) = {4};
Physical Surface(4) = {6};

Physical Surface(2) = {5};
Physical Surface(5) = {3};

Physical Surface(6) = {7};

Physical Surface(10) = {8};
Physical Surface(1) = {9, 15, 12, 10, 18, 16};

Surface Loop(2) = {9, 3, 8, 6, 7, 5, 4, 11, 14, 13, 19, 17};
Volume(1) = {2};

Surface Loop(3) = {10, 15, 12, 18, 16, 19, 17, 14, 11, 13};
Volume(2) = {3};

Physical Volume(1) = {1};
Physical Volume(2) = {2};
