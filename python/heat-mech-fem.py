from dolfin import *
import numpy as np
import math, argparse, sys
sys.path.append('./')
parameters["reorder_dofs_serial"] = False;
parameters["allow_extrapolation"] = True;
timer = Timer()

# ----------------
#    PARAMETERS
# ----------------
dim = 2
meshname = '../data/mesh3'

Tcount = 100
Tmax = 1.0e6
dt = Tmax/Tcount

# phase change
Tf = 0.0
Lf = 333000

# heat parameters s, w, i
rho_s, c_s, lmd_s = 2620.0,  900.0, 1.95
rho_w, c_w, lmd_w = 1000.0, 4180.0, 0.56
rho_i, c_i, lmd_i =  917.0, 2000.0, 2.24 
rho_p, c_p, lmd_p = rho_s, c_s, 1.0

# elastic parameters
E = 50*1.0e6
nu = 0.3
E_p = 1.0e5*1.0e6

Ttop = -15.0; atop = 14.0
Thot = 10.0;  ahot = 20.0
T_init = 2.0
patm = 101325.0

# ----------------
#      MESH
# ----------------
mesh = Mesh(meshname + '.xml')
bounds = MeshFunction("size_t", mesh, meshname+"_facet_region.xml")
subdoms = MeshFunction("size_t", mesh, meshname+"_physical_region.xml")
ds = Measure('ds', domain = mesh, subdomain_data = bounds)
dx = Measure('dx', domain = mesh, subdomain_data = subdoms)
print("Solve HEAT on mesh with %d cells" % mesh.num_cells())

# ----------------------------
#     MODEL BASED ON w(T)
# ----------------------------
wb = 0.1
ab  = 1.0/0.3
def w(T):
    if T >= Tf:
        return wb
    else:
        return wb*exp(ab*(T-Tf))
def dw(T):
    if T >= Tf:
        return 0.0
    else:
        return wb*ab*exp(ab*(T-Tf))

def ew(T):
    return w(T)*rho_s/rho_w
def dew(T):
    return dw(T)*rho_s/rho_w

def eip(T):
    return (wb - w(T))*rho_s/rho_i
def deip(T):
    return -dw(T)*rho_s/rho_i

def n(T):
    return (ew(T) + eip(T)) / (1.0 + ew(T) + eip(T))
def dn(T):
    g  = ew(T) + eip(T)
    dg = dew(T) + deip(T)
    h  = 1.0 + ew(T) + eip(T)
    dh = dew(T) + deip(T)
    return (dg*h - dh*g) / pow(h, 2)
 
def theta_i(T):
    return (1.0 - n(T))*eip(T)
def theta_w(T):
    return (1.0 - n(T))*ew(T)
def theta_s(T):
    return (1.0 - n(T))
 
def dTheta_i(T):
    return (1.0 - n(T))*deip(T) - eip(T)*dn(T)

def C(T):
    return ( theta_s(T)*c_s*rho_s + theta_w(T)*c_w*rho_w + theta_i(T)*c_i*rho_i )
def lmd(T):
    return ( pow(lmd_s, theta_s(T)) * pow(lmd_w, theta_w(T)) * pow(lmd_i, theta_i(T)) )
def Dd(T):
    return ( -Lf*rho_i*dTheta_i(T) )

def epsilon(v):
    return 0.5*(grad(v) + grad(v).T)
def sigma(E, v):
    lmd = nu*E/(1+nu)/(1-2*nu)
    mu = E/(1+nu)/2
    return 2.0*mu*epsilon(v) + lmd*tr(epsilon(v))*Identity(dim)
def betadn(T, Tn):
    lmd = nu*E/(1+nu)/(1-2*nu)
    mu = E/(1+nu)/2
    return (dim*lmd + 2*mu)*0.5/(1.0 - n(T)) * (n(T) - n(Tn))

class CoeffWrapper(UserExpression):
    def value_shape(self):
        return ()
    def setT(self, u, ctype):
        self.u = u
        self.ctype = ctype
    def setT2(self, u, un, ctype):
        self.u = u
        self.un = un
        self.ctype = ctype
    def eval(self, value, x):
        p = Point(x[0],x[1])
        val = self.u(p)
        value[0] = val
        if self.ctype == 'theta_w':
            value[0] = theta_w(val)
        if self.ctype == 'theta_i':
            value[0] = theta_i(val)
        if self.ctype == 'n':
            value[0] = n(val)
        if self.ctype == 'C':
            value[0] = C(val)
        if self.ctype == 'lmd':
            value[0] = lmd(val)
        if self.ctype == 'Dd':
            value[0] = Dd(val)
        if self.ctype == 'betadn':
            valn = self.un(p)
            value[0] = betadn(val, valn)

# ----------------
#      SOLVE
# ----------------
dgr = 0
Cn = CoeffWrapper(degree=dgr)
lmdn = CoeffWrapper(degree=dgr)
Ddn = CoeffWrapper(degree=dgr)
betadnn = CoeffWrapper(degree=dgr)
nn = CoeffWrapper(degree=dgr)
tin = CoeffWrapper(degree=dgr)
twn = CoeffWrapper(degree=dgr)
nrm = FacetNormal(mesh)

# heat 
P1 = FunctionSpace(mesh, "CG", 1)
T = TrialFunction(P1)
r = TestFunction(P1)
Tn = Function(P1)
sol = Function(P1); sol.rename('u', '0')
ss = Function(P1)

# elastic
V = VectorFunctionSpace(mesh, "CG", 1)
u = TrialFunction(V)
v = TestFunction(V)
un = Function(V)
solu = Function(V); solu.rename('u', '0')

bcu1 = DirichletBC(V.sub(0), Constant(0.0), bounds, 4)
bcu2 = DirichletBC(V.sub(0), Constant(0.0), bounds, 3)
bcu3 = DirichletBC(V.sub(1), Constant(0.0), bounds, 2)

# generate matrix for elasticity
au = inner(sigma(E, u), epsilon(v))*dx(1) + inner(sigma(E_p, u), epsilon(v))*dx(2)
Au = assemble(au)
bcu1.apply(Au)
bcu2.apply(Au)
bcu3.apply(Au)

# files to save
fileT = File('./results/fem-h.pvd')
fileu = File('./results/fem-el.pvd')
file_thi = File('./results/fem-thi.pvd')
file_thw = File('./results/fem-thw.pvd')
file_por = File('./results/fem-por.pvd')
file_dd  = File('./results/fem-dd.pvd')
   
# initial condition
sol.interpolate(Constant(T_init))

# solve         
timer.start()
t = 0; tcounter = 0
for tcounter in range(Tcount+1):
    # ----- heat system -----
    Cn.setT(sol, 'C')
    lmdn.setT(sol, 'lmd')
    Ddn.setT(sol, 'Dd')
    Tn = sol.copy(deepcopy=True)
    a = (Cn + Ddn)/dt*T*r*dx(1) + inner(lmdn*grad(T), grad(r))*dx(1)\
        + c_p*rho_p/dt*T*r*dx(2) + inner(lmd_p*grad(T), grad(r))*dx(2)\
        + atop*T*r*ds(1) + ahot*T*r*ds(10)
        
    L = (Cn + Ddn)/dt*Tn*r*dx(1) + c_p*rho_p/dt*Tn*r*dx(2)\
        + atop*Ttop*r*ds(1) + ahot*Thot*r*ds(10)
    A = assemble(a)
    b = assemble(L)
    # ----- heat solve -----
    solve(A,  sol.vector(), b, "default", "default")
    fileT << sol
    
    # ----- elasticity system -----
    betadnn.setT2(sol, Tn, 'betadn')
    un = solu.copy(deepcopy=True)
    Lu = inner(sigma(E, un), epsilon(v))*dx(1)\
        + inner(sigma(E_p, un), epsilon(v))*dx(2)\
        + inner(betadnn*Identity(dim), epsilon(v))*dx(1)
    bu = assemble(Lu)
    bcu1.apply(bu)
    bcu2.apply(bu)
    bcu3.apply(bu)
    # ----- elasticity solve -----
    solve(Au,  solu.vector(), bu, "default", "default")
    fileu << solu

    # ----- additional -----
    twn.setT(sol, 'theta_w'); tin.setT(sol, 'theta_i'); nn.setT(sol, 'n')
    ss.interpolate(twn);  ss.rename('u', '0'); file_thw << ss
    print('tw(%g, %g)' % (ss.vector().min(), ss.vector().max()))
    ss.interpolate(tin); ss.rename('u', '0'); file_thi << ss
    print('ti(%g, %g)' % (ss.vector().min(), ss.vector().max()))
    ss.interpolate(nn); ss.rename('u', '0'); file_por << ss
    print('por(%g, %g)' % (ss.vector().min(), ss.vector().max()))
    ss.interpolate(Ddn); ss.rename('u', '0'); file_dd << ss
    print('dd(%g, %g)' % (ss.vector().min(), ss.vector().max()))
    
    print('Time[%d] = %f: T(%g, %g) and u(%g, %g)\n' % (tcounter, t, sol.vector().min(), sol.vector().max(),
                                                                       solu.vector().min(), solu.vector().max()))
    t += dt
    tcounter += 1

print("Solution time %g sec" % timer.stop())
